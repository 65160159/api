import { Module } from '@nestjs/common';
import { TempuratureController } from './tempurature.controller';
import { TempuratureService } from './tempurature.service';

@Module({
  imports: [],
  exports: [],
  controllers: [TempuratureController],
  providers: [TempuratureService],
})
export class TempuratureModule {}
