import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { TempuratureService } from './tempurature.service';

@Controller('tempurature')
export class TempuratureController {
  constructor(private readonly tempuratureService: TempuratureService) {}
  @Get('convert')
  convert(@Query('celsius') celsius: string) {
    return this.tempuratureService.convert(parseFloat(celsius));
  }

  @Get('convert/:celsius')
  convertParam(@Param('celsius') celsius: string) {
    return {
      celsius: parseFloat(celsius),
      farenheit: (parseFloat(celsius) * 9.0) / 5 + 32,
    };
  }

  @Post('convert')
  convertByPost(@Body('celsius') celsius: number) {
    return this.tempuratureService.convert(celsius);
  }
}
