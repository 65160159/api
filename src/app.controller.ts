import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  Req,
  
} from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getDefault(): string {
    return 'Get Default';
  }

  @Get('hello')
  getHello(): string {
    return '<html><body><h1>green</h1></body></html>';
  }

  @Get('world')
  getWorld(): string {
    return '<html><body><h1>Hello World!!!</h1></body></html>';
  }
  @Delete('world')
  postWorld(): string {
    return '<html><body><h1>Post Hello World!!!</h1></body></html>';
  }

  @Get('test-query')
  testquery(
    @Req() req,
    @Query('celsius') celsius: number,
    @Query('type') type: string,
  ) {
    return { celsius, type };
  }

  @Get('test-params/:celsius')
  testparam(@Req() req, @Param('celsius') celsius: number) {
    return { celsius };
  }

  @Post('test-body')
  testbody(@Req() req, @Body() body, @Body('celsius') celsius: number) {
    return { celsius };
  }
}
